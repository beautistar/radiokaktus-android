package balkanmusic.radiokaktus;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Objects;

import static android.os.Build.VERSION.SDK_INT;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private String TAG = "MainActivity";
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Toolbar toolbar;
    private Fragment currentFragment;
    private TextView menu_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (SDK_INT > 8)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        android.support.v7.widget.Toolbar.LayoutParams lp = new android.support.v7.widget.Toolbar.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        drawerLayout = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        Objects.requireNonNull(actionbar).setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionbar.setCustomView(R.layout.actionbar);
        actionbar.getCustomView().setLayoutParams(lp);
        menu_title = (TextView) actionbar.getCustomView().findViewById(R.id.menu_title);
        actionbar.getCustomView().findViewById(R.id.menu_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        gotoRadio();
    }

    public void gotoRadio() {
        navigationView.getMenu().getItem(0).setChecked(true);
        menu_title.setText("");

        currentFragment = RadioFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, currentFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void gotoInvite() {
        menu_title.setText("Invite Friend");
        currentFragment = InviteFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, currentFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void gotoNewsSerbia() {
        menu_title.setText("News-Serbia");
        currentFragment = RSSNewsSerbiaFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, currentFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void gotoNewsCroatia() {
        menu_title.setText("News-Croatia");
        currentFragment = RSSNewsCroatiaFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, currentFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void gotoNewsBosnia() {
        menu_title.setText("News-Bosnia");
        currentFragment = RSSNewsBosniaFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, currentFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void openFacebook() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/radiokaktus1/"));
        startActivity(browserIntent);
    }

    public void openPrivacy() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://radiokaktusaz.wixsite.com/radiokaktus/privacy"));
        startActivity(browserIntent);
    }

    @Override
    public void onResume() {
        super.onResume();

        gotoRadio();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.nav_radio:
                gotoRadio();
                break;
            case R.id.nav_invite:
                gotoInvite();
                break;
            case R.id.nav_news_serbia:
                gotoNewsSerbia();
                break;
            case R.id.nav_news_croatia:
                gotoNewsCroatia();
                break;
            case R.id.nav_news_bosnia:
                gotoNewsBosnia();
                break;
            case R.id.nav_facebook:
                openFacebook();
                break;
//            case R.id.nav_terms:
//                break;
            case R.id.nav_privacy:
                openPrivacy();
                break;
        }
        drawerLayout.closeDrawers();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (currentFragment instanceof RadioFragment) {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
        }
        if (currentFragment instanceof InviteFragment)
            gotoRadio();
        if (currentFragment instanceof RSSNewsSerbiaFragment)
            gotoRadio();
    }
}
