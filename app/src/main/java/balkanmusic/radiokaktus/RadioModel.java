package balkanmusic.radiokaktus;

import java.io.Serializable;

/**
 * Created by dragon on 4/28/2017.
 */

public class RadioModel implements Serializable{

    private int id;
    private String title;
    private String url;
    private int logo;
    private int status;

    public void setId(int id){
        this.id = id;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public void setLogo(int logo){
        this.logo = logo;
    }

    public void setStatus(int status){
        this.status = status;
    }

    public int getId(){
        return this.id;
    }

    public String getTitle(){
        return this.title;
    }

    public String getUrl(){
        return this.url;
    }

    public int getLogo(){
        return this.logo;
    }

    public int getStatus(){
        return this.status;
    }


}
