package balkanmusic.radiokaktus;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.devbrackets.android.playlistcore.data.PlaybackState;
import com.devbrackets.android.playlistcore.listener.PlaylistListener;
import com.wang.avi.AVLoadingIndicatorView;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import balkanmusic.radiokaktus.data.MediaItem;
import balkanmusic.radiokaktus.data.Samples;
import balkanmusic.radiokaktus.manager.PlaylistManager;

public class RadioFragment extends Fragment implements PlaylistListener<MediaItem> {

    private OnFragmentInteractionListener mListener;

    private ImageView play_pause;
    private ItemAdapter adapter;
    private LinearLayout ll_loading, ll_title;

    IcyStreamMeta streamMeta;
    MetadataTask2 metadataTask2;
    TextView textView;
    ProgressBar simpleProgressBar;

    private PlaylistManager playlistManager;
    private boolean firstTime = true;

    private int mInterval = 5000; // 5 seconds by default, can be changed later
    private Handler mHandler;
    private Timer timer = new Timer();
    private MyTimerTask task = new MyTimerTask();

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                updateTitle(); //this function can change value of mInterval.
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    public RadioFragment() {
        // Required empty public constructor
    }

    public static RadioFragment newInstance() {
        RadioFragment fragment = new RadioFragment();
        return fragment;
    }

    @SuppressLint("InvalidWakeLockTag")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mHandler = new Handler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_radio, container, false);

        textView = (TextView) view.findViewById(R.id.main_activity_text_view);
        simpleProgressBar = (ProgressBar) view.findViewById(R.id.simpleProgressBar);

        ll_loading = (LinearLayout) view.findViewById(R.id.ll_loading);
        ll_title = (LinearLayout) view.findViewById(R.id.ll_title);

        final ListView listview = (ListView) view.findViewById(R.id.list_view);

        adapter = new ItemAdapter(view.getContext(), RadioKaktusApplication.getRadio());
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {

                if(RadioKaktusApplication.getCurrentRadio() == position) {
                    return;
                }

                mHandler.removeCallbacks(mStatusChecker);
                timer.cancel();

                RadioKaktusApplication.setCurrentRadio(position);
                updatePlayItems();
                if (RadioKaktusApplication.getStatus())
                    startPlayback();
                else
                    firstTime = true;
            }
        });

        play_pause = (ImageView) view.findViewById(R.id.play_pause);
        play_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (firstTime) {
                    startPlayback();
                    firstTime = false;
                } else {
                    if (RadioKaktusApplication.getStatus())
                        ll_title.setVisibility(View.INVISIBLE);
                    playlistManager.invokePausePlay();
                }
            }
        });

        setupPlaylistManager();



        if (RadioKaktusApplication.getStatus()) {
            play_pause.setImageResource(R.drawable.pause_btn);
            textView.setText(RadioKaktusApplication.getTitleArtist());
            ll_title.setVisibility(View.VISIBLE);
        }

        return view;
    }

    private void startPlayback() {
        if (!networkCheck())
            return;
        //If we are changing audio files, or we haven't played before then start the playback
        playlistManager.setCurrentPosition(RadioKaktusApplication.getCurrentRadio());
        playlistManager.play(0, false);

        updateStream();
    }

    private void updateStream() {
        streamMeta = new IcyStreamMeta();
        try {
            streamMeta.setStreamUrl(new URL(RadioKaktusApplication.getRadio().get(RadioKaktusApplication.getCurrentRadio()).getUrl()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        metadataTask2 =new MetadataTask2();
        try {
            metadataTask2.execute(new URL(RadioKaktusApplication.getRadio().get(RadioKaktusApplication.getCurrentRadio()).getUrl()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        RadioKaktusApplication.setIcyStreamMeta(streamMeta);

        timer = new Timer();
        task = new MyTimerTask();
        timer.schedule(task,1000, 5000);
        mStatusChecker.run();
    }

    private void updatePlayItems() {
        for (int i = 0; i < 3; i++) {
            if (RadioKaktusApplication.getCurrentRadio() == i) {
                if (RadioKaktusApplication.getStatus()) {
                    RadioKaktusApplication.getRadio().get(i).setStatus(2);
                } else {
                    RadioKaktusApplication.getRadio().get(i).setStatus(1);
                }
            } else {
                RadioKaktusApplication.getRadio().get(i).setStatus(0);
            }
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onPlaybackStateChanged(@NonNull PlaybackState playbackState) {
        switch (playbackState) {
            case STOPPED:
                if (RadioKaktusApplication.getStatus()) {
                    RadioKaktusApplication.setStatus(false);
                    updatePlayItems();
                    play_pause.setImageResource(R.drawable.play_btn);
                }
                break;
            case PLAYING:
                if (!RadioKaktusApplication.getStatus()) {
                    RadioKaktusApplication.setStatus(true);
                    updatePlayItems();
                    play_pause.setImageResource(R.drawable.pause_btn);
                }
                break;
            case PAUSED:
                if (RadioKaktusApplication.getStatus()) {
                    RadioKaktusApplication.setStatus(false);
                    updatePlayItems();
                    play_pause.setImageResource(R.drawable.play_btn);
                }
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public boolean onPlaylistItemChanged(@Nullable MediaItem currentItem, boolean b, boolean b1) {
        if (currentItem.getId() != RadioKaktusApplication.getCurrentRadio()) {
            mHandler.removeCallbacks(mStatusChecker);
            timer.cancel();

            RadioKaktusApplication.setCurrentRadio((int) currentItem.getId());
            updatePlayItems();

            updateStream();
        }

//        titleTextView.setText(currentItem != null ? currentItem.getTitle() : "");
//        subtitleTextView.setText(currentItem != null ? currentItem.getAlbum() : "");
//        descriptionTextView.setText(currentItem != null ? currentItem.getArtist() : "");
        return true;
    }

    private boolean setupPlaylistManager() {
        playlistManager = RadioKaktusApplication.getPlaylistManager();

        //There is nothing to do if the currently playing values are the same
        if (playlistManager.getId() == 3) {
            return false;
        }

        List<MediaItem> mediaItems = new LinkedList<>();
        for (Samples.Sample sample : Samples.getAudioSamples()) {
            MediaItem mediaItem = new MediaItem(sample, true);
            mediaItems.add(mediaItem);
        }

        playlistManager.setParameters(mediaItems, RadioKaktusApplication.getCurrentRadio());
        playlistManager.setId(3);

        return true;
    }

    protected class MetadataTask2 extends AsyncTask<URL, Void, IcyStreamMeta>
    {
        @Override
        protected IcyStreamMeta doInBackground(URL... urls)
        {

            try
            {
                streamMeta.refreshMeta();
            }
            catch (IOException e)
            {
                Log.e(MetadataTask2.class.toString(), e.getMessage());
            }
            return streamMeta;
        }

        @Override
        protected void onPreExecute() {
//            ll_loading.setVisibility(View.VISIBLE);
            ll_title.setVisibility(View.INVISIBLE);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(IcyStreamMeta result)
        {
            try
            {
                RadioKaktusApplication.setTitleArtist(streamMeta.getStreamTitle());
                if(RadioKaktusApplication.getTitleArtist().length()>0)
                {
                    textView.setText(RadioKaktusApplication.getTitleArtist());
//                    ll_loading.setVisibility(View.INVISIBLE);
                    ll_title.setVisibility(View.VISIBLE);
                }
            }
            catch (IOException e)
            {
                Log.e(MetadataTask2.class.toString(), e.getMessage());
            }
        }
    }

    private void updateTitle() {
//        playlistManager.getCurrentItem().setArtist(RadioKaktusApplication.getTitleArtist());
        textView.setText(RadioKaktusApplication.getTitleArtist());
    }

    class MyTimerTask extends TimerTask {
        public void run() {
            try {
                streamMeta.refreshMeta();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                String title_artist=streamMeta.getStreamTitle();
                Log.i("ARTIST TITLE", title_artist);
                RadioKaktusApplication.setTitleArtist(title_artist);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public boolean networkCheck() {
        ConnectivityManager ConnectionManager=(ConnectivityManager) RadioKaktusApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()==true )
        {
//            Toast.makeText(MainActivity.this, "Network Available", Toast.LENGTH_LONG).show();
            return true;
        }
        else
        {
            Toast.makeText(RadioKaktusApplication.getAppContext(), "Network Not Available", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        playlistManager = RadioKaktusApplication.getPlaylistManager();
        playlistManager.registerPlaylistListener(this);

        if (RadioKaktusApplication.getStatus()) {
            updatePlayItems();

            play_pause.setImageResource(R.drawable.pause_btn);
            textView.setText(RadioKaktusApplication.getTitleArtist());
            ll_title.setVisibility(View.VISIBLE);

            streamMeta = RadioKaktusApplication.getIcyStreamMeta();
            timer = new Timer();
            task = new MyTimerTask();
            timer.schedule(task,1000, 5000);
            mStatusChecker.run();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        playlistManager.unRegisterPlaylistListener(this);

        mHandler.removeCallbacks(mStatusChecker);
        timer.cancel();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
