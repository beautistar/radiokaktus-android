/*
 *   Copyright 2016 Marco Gomiero
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

package balkanmusic.radiokaktus;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.prof.rssparser.Article;
import com.prof.rssparser.OnTaskCompleted;
import com.prof.rssparser.Parser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainViewModel extends ViewModel {

    private MutableLiveData<List<Article>> articleListLive = null;
    private String urlString = "";

    URL url;
    ArrayList<String> headlines = new ArrayList();
    ArrayList<String> links = new ArrayList();
    ArrayList<String> descriptions = new ArrayList();
    ArrayList<String> categories = new ArrayList();
    ArrayList<String> pubDates = new ArrayList();
    ArrayList<String> medias = new ArrayList();
    ArrayList<String> guids = new ArrayList();

    List<Article> list;

    public MainViewModel(String urlString) {
        this.urlString = urlString;
    }

    private MutableLiveData<String> snackbar = new MutableLiveData<>();

    public MutableLiveData<List<Article>> getArticleList() {
        if (articleListLive == null) {
            articleListLive = new MutableLiveData<>();
        }
        return articleListLive;
    }

    private void setArticleList(List<Article> articleList) {
        this.articleListLive.postValue(articleList);
    }

    public LiveData<String> getSnackbar() {
        return snackbar;
    }

    public void onSnackbarShowed() {
        snackbar.setValue(null);
    }

    public void fetchFeed(int index) {

        if (index == 0 || index == 1) {
            Parser parser = new Parser();

            parser.onFinish(new OnTaskCompleted() {

                //what to do when the parsing is done
                @Override
                public void onTaskCompleted(List<Article> list) {
                    Log.d("Article", list.get(0).toString());
                    setArticleList(list);
                }

                //what to do in case of error
                @Override
                public void onError(Exception e) {
                    setArticleList(new ArrayList<Article>());
                    e.printStackTrace();
                    snackbar.postValue("An error has occurred. Please try again");
                }
            });
            parser.execute(urlString);

        }

        if (index == 2) {
            try {
                url = new URL(urlString);

                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(false);
                XmlPullParser xpp = factory.newPullParser();

                // We will get the XML from an input stream
                xpp.setInput(getInputStream(url), "UTF_8");

                /* We will parse the XML content looking for the "<title>" tag which appears inside the "<item>" tag.
                 * However, we should take in consideration that the rss feed name also is enclosed in a "<title>" tag.
                 * As we know, every feed begins with these lines: "<channel><title>Feed_Name</title>...."
                 * so we should skip the "<title>" tag which is a child of "<channel>" tag,
                 * and take in consideration only "<title>" tag which is a child of "<item>"
                 *
                 * In order to achieve this, we will make use of a boolean variable.
                 */
                boolean insideItem = false;

                // Returns the type of current event: START_TAG, END_TAG, etc..
                int eventType = xpp.getEventType();
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if (eventType == XmlPullParser.START_TAG) {
                        Log.d("name===>", xpp.getName());
                        if (xpp.getName().equalsIgnoreCase("item")) {
                            insideItem = true;
                        } else if (xpp.getName().equalsIgnoreCase("title")) {
                            if (insideItem)
                                headlines.add(xpp.nextText()); //extract the headline
                        } else if (xpp.getName().equalsIgnoreCase("link")) {
                            if (insideItem)
                                links.add(xpp.nextText()); //extract the link of article
                        } else if (xpp.getName().equalsIgnoreCase("guid")) {
                            if (insideItem)
                                guids.add(xpp.nextText()); //extract the link of article
                        } else if (xpp.getName().equalsIgnoreCase("description")) {
                            if (insideItem)
                                descriptions.add(xpp.nextText()); //extract the link of article
                        } else if (xpp.getName().equalsIgnoreCase("category")) {
                            if (insideItem)
                                categories.add(xpp.nextText()); //extract the link of article
                        } else if (xpp.getName().equalsIgnoreCase("pubDate")) {
                            if (insideItem)
                                pubDates.add(xpp.nextText()); //extract the link of article
                        } else if (xpp.getName().contains("media:content")) {
                            if (insideItem)
                                medias.add(xpp.getAttributeValue(1)); //extract the link of article
                        }

                    } else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("item")) {
                        insideItem = false;
                    }

                    eventType = xpp.next(); //move to next element
                }

                ArrayList<Article> articles = new ArrayList<Article>();

                for (int i = 0; i < headlines.size(); i++) {
                    Article article = new Article();

                    article.setTitle(headlines.get(i));
                    article.setLink(links.get(i));
                    article.setGuid(guids.get(i));
                    article.setDescription(descriptions.get(i));
                    article.addCategory(categories.get(i));
                    article.setPubDate(pubDates.get(i));
                    article.setImage(medias.get(i));

                    articles.add(article);
                }

                setArticleList(articles);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public InputStream getInputStream(URL url) {
        try {
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            return null;
        }
    }
}
