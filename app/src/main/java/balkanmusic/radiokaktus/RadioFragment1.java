package balkanmusic.radiokaktus;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.extractor.ExtractorSampleSource;
import com.google.android.exoplayer.upstream.Allocator;
import com.google.android.exoplayer.upstream.DataSource;
import com.google.android.exoplayer.upstream.DefaultAllocator;
import com.google.android.exoplayer.upstream.DefaultUriDataSource;
import com.google.android.exoplayer.util.Util;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

public class RadioFragment1 extends Fragment {

    private OnFragmentInteractionListener mListener;

    MediaMetadataRetriever mediaMetadataRetriever = null;

    private String currentUrl = "http://usa10.fastcast4u.com:5470/stream";

    private ExoPlayer exoPlayer;
    private ImageView play_pause;
    private ItemAdapter adapter;
    private PlayBackground player;
    private AVLoadingIndicatorView avLoadingIndicatorView;
    private TextView loading_txt;
    private LinearLayout ll_loading, ll_title;
    Button btnShare;
    private static final int BUFFER_SEGMENT_SIZE = 64 * 1024;
    private static final int BUFFER_SEGMENT_COUNT = 256;
    MediaPlayer mp;
    ProgressDialog pd;

    IcyStreamMeta streamMeta;
    MetadataTask2 metadataTask2;
    TextView textView;
    ProgressBar simpleProgressBar;

    private int mInterval = 5000; // 5 seconds by default, can be changed later
    private Handler mHandler;
    private Timer timer = new Timer();
    private MyTimerTask task = new MyTimerTask();

    PowerManager.WakeLock wl;

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                updateTitle(); //this function can change value of mInterval.
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    public RadioFragment1() {
        // Required empty public constructor
    }

    public static RadioFragment1 newInstance() {
        RadioFragment1 fragment = new RadioFragment1();
        return fragment;
    }

    @SuppressLint("InvalidWakeLockTag")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PowerManager pm = (PowerManager) this.getContext().getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        wl.acquire();

// screen and CPU will stay awake during this section

        wl.release();

        mHandler = new Handler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_radio, container, false);

        textView = (TextView) view.findViewById(R.id.main_activity_text_view);
        simpleProgressBar = (ProgressBar) view.findViewById(R.id.simpleProgressBar);

        ll_loading = (LinearLayout) view.findViewById(R.id.ll_loading);
        ll_title = (LinearLayout) view.findViewById(R.id.ll_title);

        final ListView listview = (ListView) view.findViewById(R.id.list_view);

        adapter = new ItemAdapter(view.getContext(), RadioKaktusApplication.getRadio());
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {

                if (RadioKaktusApplication.getCurrentRadio() != position) {
                    mHandler.removeCallbacks(mStatusChecker);
                    timer.cancel();

                    currentUrl = RadioKaktusApplication.getRadio().get(position).getUrl();
                    Log.e("currentUrl",""+currentUrl);


                    RadioKaktusApplication.setCurrentRadio(position);

                    exoPlayer.stop();
                    exoPlayer.release();
                    exoPlayer = null;
                    exoPlayer = ExoPlayer.Factory.newInstance(1);

                    for (int i = 0; i < 3; i++) {
                        if (position == i) {

                            if (RadioKaktusApplication.getStatus()) {
                                RadioKaktusApplication.getRadio().get(i).setStatus(2);
//                            exoPlayer.stop();
                                exoPlayer(view, currentUrl);

                            } else {
                                RadioKaktusApplication.getRadio().get(i).setStatus(1);
                            }

                        } else {
                            RadioKaktusApplication.getRadio().get(i).setStatus(0);
                        }
                    }

                    adapter.notifyDataSetChanged();

                    RadioKaktusApplication.setExoPlayer(exoPlayer);
                    RadioKaktusApplication.setPlayer(player);
                }
            }
        });

        play_pause = (ImageView) view.findViewById(R.id.play_pause);
        play_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentUrl.equals("")) {
                } else {
                    exoPlayer(view, currentUrl);
                }
            }
        });

        exoPlayer = RadioKaktusApplication.getExoPlayer();
        player = RadioKaktusApplication.getPlayer();

        if (RadioKaktusApplication.getStatus()) {
            currentUrl = RadioKaktusApplication.getRadio().get(RadioKaktusApplication.getCurrentRadio()).getUrl();
            play_pause.setImageResource(R.drawable.pause_btn);
            textView.setText(RadioKaktusApplication.getTitleArtist());
            ll_title.setVisibility(View.VISIBLE);
        }

        return view;
    }

    public void exoPlayer(View view, String radioChannelUrl){

        Uri radioUri = Uri.parse(radioChannelUrl);
        // Settings for exoPlayer
        Allocator allocator = new DefaultAllocator(BUFFER_SEGMENT_SIZE);
        String userAgent = Util.getUserAgent(view.getContext(), "ExoPlayerDemo");

        DataSource dataSource = new DefaultUriDataSource(view.getContext(), null, userAgent);
        ExtractorSampleSource sampleSource = new ExtractorSampleSource(radioUri, dataSource, allocator, BUFFER_SEGMENT_SIZE * BUFFER_SEGMENT_COUNT);

        String str1 = sampleSource.toString();
        Log.e("str3","" + str1);
        MediaCodecAudioTrackRenderer audioRenderer = new MediaCodecAudioTrackRenderer(sampleSource);

        // Prepare ExoPlayer
        exoPlayer.prepare(audioRenderer);

        if (exoPlayer != null && exoPlayer.getPlayWhenReady()) {
            mHandler.removeCallbacks(mStatusChecker);
            timer.cancel();

            exoPlayer.stop();
            play_pause.setImageResource(R.drawable.play_btn);
            ll_loading.setVisibility(View.INVISIBLE);
            ll_title.setVisibility(View.INVISIBLE);
            textView.setText("");

            player.onCancelled();
            RadioKaktusApplication.getRadio().get(RadioKaktusApplication.getCurrentRadio()).setStatus(1);
            adapter.notifyDataSetChanged();
            RadioKaktusApplication.setStatus(false);
            exoPlayer = player.getExoPlayer();

            if (wl.isHeld()) {
                wl.release();
            }
        }else {
            if (!networkCheck(view))
                return;
            player = new PlayBackground(exoPlayer, true);
            player.execute();
            play_pause.setImageResource(R.drawable.pause_btn);
            ll_loading.setVisibility(View.VISIBLE);
            ll_title.setVisibility(View.INVISIBLE);
            RadioKaktusApplication.getRadio().get(RadioKaktusApplication.getCurrentRadio()).setStatus(2);
            adapter.notifyDataSetChanged();
            RadioKaktusApplication.setStatus(true);
            exoPlayer = player.getExoPlayer();
            streamMeta = new IcyStreamMeta();
            try {
                streamMeta.setStreamUrl(new URL(radioChannelUrl));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            metadataTask2 =new MetadataTask2();
            try {
                metadataTask2.execute(new URL(radioChannelUrl));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            RadioKaktusApplication.setIcyStreamMeta(streamMeta);

            timer = new Timer();
            task = new MyTimerTask();
            timer.schedule(task,1000, 5000);
            mStatusChecker.run();

            wl.acquire();
        }

        RadioKaktusApplication.setExoPlayer(exoPlayer);
        RadioKaktusApplication.setPlayer(player);
    }

    protected class MetadataTask2 extends AsyncTask<URL, Void, IcyStreamMeta>
    {
        @Override
        protected IcyStreamMeta doInBackground(URL... urls)
        {

            try
            {
                streamMeta.refreshMeta();
            }
            catch (IOException e)
            {
                Log.e(MetadataTask2.class.toString(), e.getMessage());
            }
            return streamMeta;
        }

        @Override
        protected void onPreExecute() {
            ll_loading.setVisibility(View.VISIBLE);
            ll_title.setVisibility(View.INVISIBLE);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(IcyStreamMeta result)
        {
            try
            {
                RadioKaktusApplication.setTitleArtist(streamMeta.getStreamTitle());
                if(RadioKaktusApplication.getTitleArtist().length()>0)
                {
                    textView.setText(RadioKaktusApplication.getTitleArtist());
                    ll_loading.setVisibility(View.INVISIBLE);
                    ll_title.setVisibility(View.VISIBLE);
                }
            }
            catch (IOException e)
            {
                Log.e(MetadataTask2.class.toString(), e.getMessage());
            }
        }
    }

    private void updateTitle() {
        textView.setText(RadioKaktusApplication.getTitleArtist());
    }

    class MyTimerTask extends TimerTask {
        public void run() {
            try {
                streamMeta.refreshMeta();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                String title_artist=streamMeta.getStreamTitle();
                Log.i("ARTIST TITLE", title_artist);
                RadioKaktusApplication.setTitleArtist(title_artist);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public boolean networkCheck(View view) {
        ConnectivityManager ConnectionManager=(ConnectivityManager) view.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()==true )
        {
//            Toast.makeText(MainActivity.this, "Network Available", Toast.LENGTH_LONG).show();
            return true;
        }
        else
        {
            Toast.makeText(view.getContext(), "Network Not Available", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (RadioKaktusApplication.getStatus()) {
            streamMeta = RadioKaktusApplication.getIcyStreamMeta();
            timer = new Timer();
            task = new MyTimerTask();
            timer.schedule(task,1000, 5000);
            mStatusChecker.run();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacks(mStatusChecker);
        timer.cancel();
        if (wl.isHeld()) {
            wl.release();
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
