package balkanmusic.radiokaktus;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.exoplayer.ExoPlayer;

/**
 * Created by dragon on 11/24/2016.
 */

public class PlayBackground extends AsyncTask<String, String, String>{

    private ExoPlayer exoPlayer;
    private boolean play;
    /**
     * Before starting background thread Show Progress Bar Dialog
     * */
    public PlayBackground(ExoPlayer exoPlayer, boolean play){
        this.exoPlayer = exoPlayer;
        this.play = play;
    }

    public ExoPlayer getExoPlayer(){
        return this.exoPlayer;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();



//        mContext.showDialog(progress_bar_type);
    }

    /**
     * Downloading file in background thread
     * */
    @Override
    protected String doInBackground(String... f_url) {
        try {

            exoPlayer.setPlayWhenReady(play);

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }

        return null;
    }

    /**
     * Updating progress bar
     * */
//    protected void onProgressUpdate(String... progress) {
//        // setting progress percentage
//        pDialog.setProgress(Integer.parseInt(progress[0]));
//    }

    /**
     * After completing background task Dismiss the progress dialog
     * **/
    @Override
    protected void onPostExecute(String file_url) {
        // dismiss the dialog after the file was downloaded
//        mContext.dismissDialog(progress_bar_type);

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        exoPlayer.setPlayWhenReady(false);
    }
}
