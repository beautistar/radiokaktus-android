package balkanmusic.radiokaktus;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

public class InviteFragment extends Fragment {
    private OnFragmentInteractionListener mListener;

    public InviteFragment() {
        // Required empty public constructor
    }

    public static InviteFragment newInstance() {
        InviteFragment fragment = new InviteFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_invite, container, false);

        view.findViewById(R.id.img_invite_email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = ((EditText) view.findViewById(R.id.edt_email)).getText().toString();
                if (email.length() > 5 && email.contains("@") && email.contains(".")) {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", "abc@mail.com", null));
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, "address");
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Recommend");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "Please download Radio Kaktus app from App/Play store.It's FREE.");
                    startActivity(Intent.createChooser(emailIntent, "Send Email..."));
                } else {
                    Toast.makeText(view.getContext(), "Please input correct Email Address", Toast.LENGTH_LONG).show();
                }
            }
        });

        view.findViewById(R.id.img_invite_social).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "Please download Radio Kaktus app from App/Play store.It's FREE.");
                startActivity(Intent.createChooser(intent, "Share via"));
            }
        });

        return view;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
