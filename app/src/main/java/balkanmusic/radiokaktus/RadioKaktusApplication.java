package balkanmusic.radiokaktus;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.google.android.exoplayer.ExoPlayer;

import java.util.ArrayList;

import balkanmusic.radiokaktus.manager.PlaylistManager;

public class RadioKaktusApplication extends Application {
    public static final String TAG = RadioKaktusApplication.class.getSimpleName();

    public static MainActivity _mainActivity;

    private static RadioKaktusApplication _application;

    private static PlaylistManager _playlistManager;

    private static ExoPlayer _exoPlayer;

    private static PlayBackground _player;

    private static int _currentRadio = 0;

    private static String _title_artist = "";

    private static boolean _status;

    private static IcyStreamMeta _icyStreamMeta;

    private static ArrayList<RadioModel> _radio = new ArrayList<>();
    private RadioModel item1 = new RadioModel();
    private RadioModel item2 = new RadioModel();
    private RadioModel item3 = new RadioModel();

    @Override

    public void onCreate() {

        super.onCreate();
        _application = this;
        _playlistManager = new PlaylistManager(this);

        item1.setId(0);
        item1.setTitle("Radio Kaktus Top Hits");
        item1.setUrl("http://usa10.fastcast4u.com:5470/stream");
        item1.setStatus(1);
        _radio.add(item1);

        item2.setId(1);
        item2.setTitle("Radio Kaktus Pop/Rock");
        item2.setUrl("http://usa10.fastcast4u.com:1680/stream");
        item2.setStatus(0);
        _radio.add(item2);

        item3.setId(2);
        item3.setTitle("Radio Kaktus Nostalgija");
        item3.setUrl("http://usa10.fastcast4u.com:5730/stream");
        item3.setStatus(0);
        _radio.add(item3);

        _exoPlayer = ExoPlayer.Factory.newInstance(1);
        _player = new PlayBackground(_exoPlayer, false);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        _application = null;
        _playlistManager = null;
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static synchronized RadioKaktusApplication getInstance() {

        return _application;
    }

    public static void setMainActivity(MainActivity mainActivity) {
        _mainActivity = mainActivity;
    }

    public static MainActivity getMainActivity() {

        return _mainActivity;
    }

    public static PlaylistManager getPlaylistManager() {
        return _playlistManager;
    }

    public static void setExoPlayer(ExoPlayer exoPlayer) {
        _exoPlayer = exoPlayer;
    }

    public static ExoPlayer getExoPlayer() {
        return _exoPlayer;
    }

    public static void setPlayer(PlayBackground playBackground) {
        _player = playBackground;
    }

    public static PlayBackground getPlayer() {
        return _player;
    }

    public static void setCurrentRadio(int currentRadio) {
        _currentRadio = currentRadio;
    }

    public static int getCurrentRadio() {
        return _currentRadio;
    }

    public static void setStatus(boolean status) {
        _status = status;
    }

    public static boolean getStatus() {
        return _status;
    }

    public static void setTitleArtist(String title_artist) {
        _title_artist = title_artist;
    }

    public static String getTitleArtist() {
        return _title_artist;
    }

    public static void setIcyStreamMeta(IcyStreamMeta icyStreamMeta) {
        _icyStreamMeta = icyStreamMeta;
    }

    public static IcyStreamMeta getIcyStreamMeta() {
        return _icyStreamMeta;
    }

    public static ArrayList<RadioModel> getRadio() {
        return _radio;
    }

    public static Context getAppContext() {
        return _application.getApplicationContext();
    }

}