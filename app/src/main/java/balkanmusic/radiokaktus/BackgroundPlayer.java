package balkanmusic.radiokaktus;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.extractor.ExtractorSampleSource;
import com.google.android.exoplayer.upstream.Allocator;
import com.google.android.exoplayer.upstream.DataSource;
import com.google.android.exoplayer.upstream.DefaultAllocator;
import com.google.android.exoplayer.upstream.DefaultUriDataSource;
import com.google.android.exoplayer.util.Util;

/**
 * Created by dragon on 4/28/2017.
 */

public class BackgroundPlayer extends Service {

    public Context context = this;
    public Handler handler = null;
    public static Runnable runnable = null;
    private ExoPlayer exoPlayer;
    private String currentUrl = "";

    private static final int BUFFER_SEGMENT_SIZE = 64 * 1024;
    private static final int BUFFER_SEGMENT_COUNT = 256;


    @Override
    public IBinder onBind(Intent intent) {
        Bundle extras = intent.getExtras();
        exoPlayer = ExoPlayer.Factory.newInstance(1);
        currentUrl = (String) extras.get("currentUrl");
        exoPlayer(currentUrl);
        return null;
    }

    @Override
    public void onCreate() {
        Toast.makeText(this, "Service created!", Toast.LENGTH_LONG).show();



        if (currentUrl.equals("")) {
//                    Toast.makeText(MainActivity.this, "Select Radio Channel", Toast.LENGTH_SHORT).show();
        }else {
//            exoPlayer(currentUrl);
        }

        /*runnable = new Runnable() {
            public void run() {
                Toast.makeText(context, "Service is still running", Toast.LENGTH_LONG).show();
            }
        };*/


    }

    @Override
    public void onDestroy() {
        /* IF YOU WANT THIS SERVICE KILLED WITH THE APP THEN UNCOMMENT THE FOLLOWING LINE */
        //handler.removeCallbacks(runnable);
        Toast.makeText(this, "Service stopped", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStart(Intent intent, int startid) {
        Toast.makeText(this, "Service started by user.", Toast.LENGTH_LONG).show();
    }

    public void exoPlayer (String radioChannelUrl){

        Uri radioUri = Uri.parse(radioChannelUrl);

        // Settings for exoPlayer
        Allocator allocator = new DefaultAllocator(BUFFER_SEGMENT_SIZE);
        String userAgent = Util.getUserAgent(this, "ExoPlayerDemo");
        DataSource dataSource = new DefaultUriDataSource(this, null, userAgent);
        ExtractorSampleSource sampleSource = new ExtractorSampleSource(radioUri, dataSource, allocator, BUFFER_SEGMENT_SIZE * BUFFER_SEGMENT_COUNT);
        MediaCodecAudioTrackRenderer audioRenderer = new MediaCodecAudioTrackRenderer(sampleSource);
        // Prepare ExoPlayer
        exoPlayer.prepare(audioRenderer);

        if (exoPlayer != null && exoPlayer.getPlayWhenReady()) {

            exoPlayer.stop();
            exoPlayer.setPlayWhenReady(false);
//            play_pause.setImageResource(R.drawable.play_button);
//            radio.get(currentRadio).setStatus(1);
//            adapter.notifyDataSetChanged();
//            status = false;

        }else {
            exoPlayer.setPlayWhenReady(true);
//            play_pause.setImageResource(R.drawable.pause_button);
//            radio.get(currentRadio).setStatus(2);
//            adapter.notifyDataSetChanged();
//            status = true;
        }
    }
}