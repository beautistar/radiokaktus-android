package balkanmusic.radiokaktus;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by dragon on 4/28/2017.
 */

public class ItemAdapter extends ArrayAdapter<RadioModel> {
    private final Context context;
    private ArrayList<RadioModel> radioModels = new ArrayList<>();

    public ItemAdapter(Context context, ArrayList<RadioModel> radioModels) {
        super(context, -1, radioModels);
        this.context = context;
        this.radioModels = radioModels;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.radio_item, parent, false);

        RelativeLayout radio_item = (RelativeLayout) rowView.findViewById(R.id.rl_radio_item);
        TextView title = (TextView) rowView.findViewById(R.id.title);
        ImageView status = (ImageView) rowView.findViewById(R.id.status);
        ImageView logo = (ImageView) rowView.findViewById(R.id.logo);

        title.setText(radioModels.get(position).getTitle());

        if (radioModels.get(position).getStatus() == 0) {
            radio_item.setBackgroundResource(R.drawable.inactive_bar);
            status.setImageResource(R.drawable.stop_icon);
            logo.setImageResource(R.drawable.radio_graph_inactive);
        } else if (radioModels.get(position).getStatus() == 1){
            radio_item.setBackgroundResource(R.drawable.active_bar);
            status.setImageResource(R.drawable.play_icon);
            logo.setImageResource(R.drawable.radio_graph);
        } else if (radioModels.get(position).getStatus() == 2){
            radio_item.setBackgroundResource(R.drawable.active_bar);
            status.setImageResource(R.drawable.pause_icon);
            logo.setImageResource(R.drawable.radio_graph);
        }

        return rowView;
    }
}